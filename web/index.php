<?php
/**
 * This file contains the bootsraping logic for the application.
 */

// TODO: Create unit tests
// TODO: Eliminar token hardcoded por un parametro.
// TODO: Hacer guzzle request asincronas y usar sus promises

$filename = __DIR__.preg_replace('#(\?.*)$#', '', $_SERVER['REQUEST_URI']);
if (php_sapi_name() === 'cli-server' && is_file($filename)) {
    return false;
}

$app = require_once __DIR__.'/../app/app.php';

$app->run();
