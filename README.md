# About this repository #

This repository contains the implementation code for the Olapic Backend test project for Agustin Berasategui.

This Application allows you to obtain location information related to an Instagram Media element. It uses Instagram API to obtain basic location information about the desired Media Element, and using its geo point information retrieves extra location information from [GeoBytes](http://www.geobytes.com/) API. 

### Installation ###

To install this project:

Clone it to your local box using [GIT](https://git-scm.com) clone command.

```
#!bash

git clone https://ajberasategui@bitbucket.org/ajberasategui/olapicbackend2.git
```

Install it dependencies using [COMPOSER](http://getcomposer.com). Run its install command from the root directory of the project.

```
#!bash

composer install
```

Serve it using [php built in server](http://php.net/manual/es/features.commandline.webserver.php) from its root directory either by running:

```
#!bash

php -S localhost:DesiredPort -t web web/index.php
```

Hit localhost:DesiredPort with a GET request to obtain Usage instructions. 

### Usage ###
You can obtain location information for a given Instagram Media element by providing either its ID or its URL.
You can get its results in 3 different formats: HTML, JSON or XML.

To use it you will need to obtain an Instagram API access token from Instagram. First you will need to [register](https://instagram.com/developer/register/) as developer, then create an API Client App [here](https://instagram.com/developer/clients/manage/) and then use your Apps credentials to obtain an access token using any REST client of your choice, i.e:[Postman](https://chrome.google.com/webstore/detail/postman/fhbjgbiflinjbdggehcddcbncdddomop).

After obtaining your access token and having this application running as explained in installation instructions you can make different requests to it according to how you want to identify the media element and which kind of response you want to receive. The access token must be always present as query string parameter "token".

* To obtain the media location information using its ID make a GET request to localhost:DesiredPort/location/{mediaId}?token=YourAccessToken

* To obtain the media location information using its URL make a GET request to localhost:DesiredPort/location/?url=mediaURL&token=YourAccessToken

The response type is controlled by the HTTP Accept Header you send. You have 3 choices:
* To obtain a JSON formatted response set your request Accept header to application/json
* To obtain an XML formatted response set your request Accept header to application/xml
* To obtain an HTML formatted response set your request Accept header to text/html

### Usage Example ###

* The easiest way to use it is to use your browser to hit the application with a GET request. As the browser asks for a text/html response you will obtain an HTML page with the media element location information. 
So, open your browser and navigate to (example media ID provided):

```
#!bash

localhost:DesiredPort/location/914837609611565028_425596060?token=YourAccessToken
```

* Using [CURL](http://curl.haxx.se/) to obtain a JSON formatted response and identifying the media element using its URL:

```
#!bash

curl -H "Accept:application/json" -X GET "localhost:8001/location/?url=https://instagram.com/p/5FqDtqncqw/&token=yourAccessToken"
```

* Using [CURL](http://curl.haxx.se/) to obtain an XML formatted response and identifying the media element using its ID:

```
#!bash

curl -H "Accept:application/XML" -X GET "localhost:8001/location/914837609611565028_425596060?token=yourAccessToken"
```

### Tests ###

This project contains Unit Tests for its main service class Olapic\Provider\LocationServiceProvider. To run the tests just run [PHPUnit](http://phpunit.de) from its root directory:

```
#!bash

phpunit
```


### Contact Me ###

* Agustin Berasategui <ajberasategui at gmail dot com>