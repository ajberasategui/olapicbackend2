<?php
/**
 * This file is part of the Olapic Backend Test Project, and contains the
 * LocationControllerProvider controller provider class implementation.
 *
 * @author Agustin Berasategui <ajberasategui@gmail.com>
 *
 * @package Olapic\Provider;
 */

namespace Olapic\Provider;

use Symfony\Component\HttpFoundation\Request;
use Silex\Application;
use Silex\ControllerProviderInterface;
use Olapic\Provider\LocationServiceProvider;
use Olapic\Model\Media;

/**
 * This class implements the controllers for the /location/ REST URL which
 * allow external clients to obtain media element location information using
 * its ID or its URL.
 */
class LocationControllerProvider implements ControllerProviderInterface
{
    protected $checkUrlParam;
    protected $checkTokenParam;

    public function __construct()
    {
        $this->checkUrlParam = function(Request $request, Application $app)
        {
            $queryParams = $request->query->all();
            if (!isset($queryParams['url']) || empty($queryParams['url'])) {
                $app->abort(400, "URL parameter missing.");
            }
        };

        $this->checkTokenParam = function(Request $request, Application $app)
        {
            $queryParams = $request->query->all();
            if (!isset($queryParams['token']) ||
                empty($queryParams['token']))
            {
                $app->abort(400, "Token parameter missing.");
            }
        };
    }

    public function connect(Application $app)
    {
        $medias = $app['controllers_factory'];

        $medias->get(
            '/',
            'Olapic\\Provider\\LocationControllerProvider::getByUrl'
        )
        ->before($this->checkUrlParam)
        ->before($this->checkTokenParam);

        $medias->get(
            '/{id}',
            'Olapic\\Provider\\LocationControllerProvider::getById'
        )->before($this->checkTokenParam);
        return $medias;
    }

    /**
     * Retrieves media element (identified by its ID) location information.
     * @param Application $app  Main Silex Application object.
     * @param  string $id  media element ID
     * @return string      json/xml serialized media location information
     * if the Accept header was set to either of this formats.
     * @return string   rendered html twig template if accept header was
     * set to text/html
     */
    public function getById(Application $app, $id)
    {
        $queryParams = $app['request']->query->all();
        $accessToken = $queryParams['token'];
        $location = $app['location']($id, $accessToken);
        $media = new Media($id, $location);
        $locData = $media->getLocation();
        $geoPoint = $locData->getGeoPoint();
        $locExtendedData = $app['extraLocation'](
            $geoPoint['latitude'],
            $geoPoint['longitude'],
            $locData->getName(),
            $locData->getId()
        );
        $media->setLocation($locExtendedData);

        $acceptHeader = $app['request']->headers->get('Accept');
        $acceptFormat = $app['negotiator']->getBest(
            $acceptHeader,
            ['text/html', 'application/json', 'application/xml']
        );
        if (null != $acceptFormat) {
            $format = $acceptFormat->getValue();
            switch ($format) {
                case "application/json":
                    $response = [
                        'status'        => 'success',
                        'mediaLocation' => $media
                    ];
                    return $app['serializer']->serialize($response, 'json');
                    break;
                case "text/html":
                    return $app['twig']->render('location.twig', [
                        'media' => $media
                    ]);
                    break;
                case "application/xml":
                    $response = [
                        'status'        => 'success',
                        'mediaLocation' => $media
                    ];
                    return $app['serializer']->serialize($response, 'xml');
                    break;
                default:
                    $app->abort(
                        400,
                        "Request Accept format not supported"
                    );
                    break;
            }
        } else {
            $app->abort(400, "Request Accept format not supported");
        }
    }

    /**
     * Retrieves media element (identified by its URL) location information.
     * @param  Application $app Main Silex Application object.
     * @param  string      $url Media element url.
     * @return string      json/xml serialized media location information
     * if the parameter _format was included in the request and was set to
     * either of this formats.
     * @return string   rendered html twig template if not _format parameters
     * were present on the request.
     * @uses LocationControllerProvider::getById()
     */
    public function getByUrl(Application $app)
    {
        $queryParams = $app['request']->query->all();
        $id = $app['mediaId']($queryParams['url']);
        return $this->getById($app, $id);
    }
}
