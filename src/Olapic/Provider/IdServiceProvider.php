<?php
/**
 * This file is part of the Olapic Backend Test Project, and contains the
 * IdServiceProvider service provider class implementation.
 *
 * @author Agustin Berasategui <ajberasategui@gmail.com>
 *
 * @package Olapic\Provider;
 */

namespace Olapic\Provider;

use Silex\Application;
use Silex\ServiceProviderInterface;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;

/**
 * This class provides a service which allows its clients to obtain the media
 * ID for an Instagram media element using its URL.
 */
class IdServiceProvider implements ServiceProviderInterface
{
    public function register(Application $app)
    {
        $app['mediaId'] = $app->protect(function ($mediaUrl) use ($app) {
            $client = new Client([
                'synchronous'   => true
            ]);
            $oembedUrl = 'http://api.instagram.com/oembed?url=' . $mediaUrl;
            try {
                $response = $client->get($oembedUrl);
                $decoded = json_decode($response->getBody());
                return $decoded->media_id;
            } catch (ClientException $e) {
                $response = $e->getResponse();
                $errorMsg = "Error getting ID from instagram. Please check " .
                    "the URL parameter is correct. Instagram response is: " .
                    $response->getReasonPhrase();
                $app->abort(400, $errorMsg);
            }
        });
    }

    public function boot(Application $app)
    {

    }
}
