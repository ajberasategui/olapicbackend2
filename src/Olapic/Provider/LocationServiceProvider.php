<?php
/**
 * This file is part of the Olapic Backend Test Project, and contains the
 * LocationServiceProvider service provider class implementation.
 *
 * @author Agustin Berasategui <ajberasategui@gmail.com>
 *
 * @package Olapic\Provider;
 */

namespace Olapic\Provider;

use Silex\Application;
use Silex\ServiceProviderInterface;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use Olapic\Model\Location;
use Olapic\Model\ExtendedLocation;
use Olapic\Model\Media;

/**
 * This class provides interaction with Instagram API, allowing its clients to
 * to retrieve location information for a media element.
 */
class LocationServiceProvider implements ServiceProviderInterface
{
    /**
     * Silex Application.
     * @var Application
     */
    protected $app;

    /**
     * {@inheritdoc}
     *
     * This method registers two services:
     * location service which allows to retrieve basic location information for
     * a media element from Instagram API.
     * extraLocation service which allows to retrieve extra location information
     * for a given geoPoint using GeoBytes API.
     *
     * @param  Application $app
     */
    public function register(Application $app)
    {
        $this->app = $app;
        $app['location'] = $app->protect(function (
            $mediaId,
            $token
        ) use ($app) {
            return $this->getLocation(
                $app['location.endpoint'],
                $token,
                $mediaId
            );
        });

        $app['extraLocation'] = $app->protect(
            function($latitude, $longitude, $name = null, $id = null) use ($app)
        {
            return $this->getExtraLocation($latitude, $longitude, $name, $id);
        });
    }

    /**
     * {@inheritdoc}
     *
     * This provider does not execute any code when booting.
     *
     * @param  Application $app
     */
    public function boot(Application $app)
    {
    }

    /**
     * Gets Media data from Instagram API and returns its location
     * information.
     * @param  string $endpoint Instagram API endpoint.
     * @param  string $token    Instagram Access Token
     * @param  string $mediaId  Instagram Media ID assigned to media element.
     * @return ExtendedLocation           ExtendedLocation instance containing
     * the location data that could be fetched from Instagram API.
     */
    public function getLocation($endpoint, $token, $mediaId)
    {
        $mediaUri = $endpoint . $mediaId . '/?access_token=' . $token;
        try {
            $decodedResponse = $this->getLocationFromInstagram($mediaUri);
            //TODO What if location is null???
            $stdLocation = $decodedResponse->data->location;

            $location = new ExtendedLocation(
                $stdLocation->name,
                [
                    'latitude'  => $stdLocation->latitude,
                    'longitude' => $stdLocation->longitude
                ],
                $stdLocation->id
            );
            return $location;
        } catch (ClientException $e) {
            $response = $e->getResponse();
            $respBody = json_decode($response->getBody());
            $error = $respBody->meta->error_message;
            $this->app->abort(
                400,
                "Instagram responded with an error: \"" . $error . "\"" .
                ". Please check your parameters."
            );
        }
    }

    /**
     * Gets Location information for a given geo point identified by latitude
     * and longitude and returns an ExtendedLocation instance populated with
     * the the data obtained from GeoBytes API.
     * @param  string $latitude  geopoint latitude
     * @param  string $longitude geopoint longitude
     * @param  string $name
     * @param  [type] $id        [description]
     * @return [type]            [description]
     */
    public function getExtraLocation($latitude, $longitude, $name, $id)
    {
        try {
            $decodedResponse = $this->getLocationFromGeoBytes(
                $latitude,
                $longitude
            );
            $geoPoint = [
                'latitude'  => $latitude,
                'longitude' => $longitude
            ];
            $locationData = new ExtendedLocation(
                $name,
                $geoPoint,
                $id,
                $decodedResponse->geobytesinternet,
                $decodedResponse->geobytesregion,
                $decodedResponse->geobytescode,
                $decodedResponse->geobytescity,
                $decodedResponse->geobytescapital,
                $decodedResponse->geobytestimezone,
                $decodedResponse->geobytesnationalitysingular,
                $decodedResponse->geobytespopulation,
                $decodedResponse->geobytescurrency,
                $decodedResponse->geobytescurrencycode,
                $decodedResponse->geobytestitle,
                $decodedResponse->geobytesfqcn
            );
            return $locationData;
        } catch (ClientException $e) {
            $response = $e->getResponse();
            $respBody = json_decode($response->getBody());
            $error = $respBody->meta->error_message;
            $this->app->abort(
                400,
                "Instagram responded with an error: \"" . $error . "\"" .
                ". Please check your parameters."
            );
        }
    }

    /**
     * Makes a Request to Instagram API using Guzzle Client to retrieve the
     * media information corresponding to the media element identified by
     * $mediaUri.
     *
     * @param  string $mediaUri Instagram url where to make the request.
     *
     * @return StdClass           Object containing the Instagram API JSON
     * decoded media data response.
     */
    protected function getLocationFromInstagram($mediaUri)
    {
        $client = new Client([
            'synchronous'   => true
        ]);
        $response = $client->get($mediaUri);
        $decodedResponse = json_decode($response->getBody());
        return $decodedResponse;
    }

    /**
     * Makes a request to GeoBytes API for location information related to
     * the geopoint represented by $latitude and $longitude.
     *
     * @param  string $latitude  Geopoint latitude
     * @param  string $longitude Geopoint longitude
     *
     * @return StdClass            Object containing the GeoBytes API JSON 
     * decoded media data response.
     */
    protected function getLocationFromGeoBytes($latitude, $longitude)
    {
        $uri = "http://getcitydetails.geobytes.com/GetCityDetails" .
            "?latitude=" . $latitude .
            "&longitude=" . $longitude;
        $client = new Client(['synchronous' => true]);
        $response = $client->get($uri);
        $decodedResponseBody = json_decode($response->getBody());
        return $decodedResponseBody;
    }
}
