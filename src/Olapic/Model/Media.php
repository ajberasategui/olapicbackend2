<?php
/**
 * This file contains the implementation of the Media model class.
 *
 * @author Agustin Berasategui <ajberasategui@gmail.com>
 *
 * @package Olapic\Model
 */

namespace Olapic\Model;

/**
 * Model to represent an Instagram media element.
 */
class Media {
    /**
     * id assigned by Instagram to the media element.
     * @var string
     */
    private $mediaID;
    /**
     * Instagram media URL
     * @var string
     */
    private $imageUrl;
    /**
     * Media location information
     * @var Olapic\Model\Location
     */
    private $location;

    /**
     * Creates a new Media class instance
     * @param string $mediaID          Id assigned by Instagram to the media
     * @param Olapic\Model\Location $location      optional Media Location data.
     * @param string $imageUrl         (optional) Media URL
     */
    public function __construct(
        $mediaID,
        $location,
        $imageUrl = null
    ) {
        $this->mediaID = $mediaID;
        $this->imageUrl = $imageUrl;
        $this->location = $location;
    }

    public function getMediaID()
    {
        return $this->mediaID;
    }

    public function setMediaID(String $mediaID)
    {
        $this->mediaID = $mediaID;

        return $this;
    }

    public function getimageUrl()
    {
        return $this->imageUrl;
    }

    public function setimageUrl(String $imageUrl)
    {
        $this->imageUrl = $imageUrl;

        return $this;
    }

    public function getLocation()
    {
        return $this->location;
    }

    public function setLocation(Location $location)
    {
        $this->location = $location;

        return $this;
    }
}
