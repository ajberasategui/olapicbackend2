<?php
/**
 * This file contains the implementation of the media Location model class.
 *
 * @author Agustin Berasategui <ajberasategui@gmail.com>
 *
 * @package Olapic\Model
 */

namespace Olapic\Model;

/**
 * Model representing the Location information for a Instagram media
 * element.
 */
class Location
{
    /**
     * location name
     * @var string
     */
    private $name = null;

    /**
     * geopoint array representation. Contains latitude and longitude.
     * @var array
     */
    private $geoPoint = [
        "latitude"  => null,
        "longitude" => null
    ];

    /**
     * id assigned by Instagram to the location.
     * @var string
     */
    private $id = null;

    /**
     * Creates a new location instance.
     * @param string $name     name assigned by Instagram API to the location.
     * @param array  $geoPoint assoc array containing latitude and longitude.
     * @param string $id       id assigned by Instagram API to the location.
     */
    public function __construct($name, $geoPoint, $id)
    {
        $this->name = $name;
        $this->geoPoint = $geoPoint;
        $this->id = $id;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getGeoPoint()
    {
        return $this->geoPoint;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    public function setGeoPoint($geoPoint)
    {
        $this->geoPoint = $geoPoint;
    }

    public function setId($id)
    {
        $this->id = $id;
    }
}
