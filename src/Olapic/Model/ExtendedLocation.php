<?php
/**
 * This file contains the implementation of the media Location model class.
 *
 * @author Agustin Berasategui <ajberasategui@gmail.com>
 *
 * @package Olapic\Model
 */

namespace Olapic\Model;

/**
 * Represents extended media location information.
 *
 * {@inheritdoc} It extends the parent Location class with extra location
 * attributes, obtained from GeoBytes API.
 *
 * @link http://www.geobytes.com/
 */
class ExtendedLocation extends Location
{
    /**
     * Represents the location country code.
     * @var string
     */
    private $countryCode = null;
    /**
     * Name of the region for the location.
     * @var string
     */
    private $region = null;
    /**
     * A code representing the region for the location.
     * @var string
     */
    private $regionCode = null;
    /**
     * Name of the city the location is in or near.
     * @var string
     */
    private $city = null;
    /**
     * Name of the capital of the region.
     * @var string`
     */
    private $regionCapital = null;
    /**
     * Location region timezone.
     * @var string
     */
    private $timezone = null;
    /**
     * Nationality name for the people living in the location region.
     * @var string
     */
    private $nationality = null;
    /**
     * Region population.
     * @var string | integer
     */
    private $population = null;
    /**
     * Name of currency used in the location region.
     * @var string
     */
    private $currency = null;
    /**
     * Code for the currency.
     * @var string
     */
    private $currencyCode = null;
    /**
     * Country name where the location is at.
     * @var string
     */
    private $countryTitle = null;
    /**
     * Fully qualified name for the location.
     */
    private $fqn = null;

    /**
     * Creates a new ExtendedLocation instance.
     *
     * @param string $name          Location name
     * @param array $geoPoint       array containing latitude and longitude
     * @param string $id            id assigned by Instagram for the location
     * @param string $countryCode   optional Code representing the location country
     * @param string $region        optional Location region name
     * @param string $regionCode    optional Code representing the location region
     * @param string $city          optional Name of the city the location is at or near
     * @param string $regionCapital optional Name of the capital city of the region
     * @param string $timezone      optional Location timezone
     * @param string $nationality   optional Nationality name for the people
     * living at this country/region.
     * @param string | integer $population    optional Region population
     * @param string $currency      optional Name of currency used in the location
     * @param string $currencyCode  optional Currency code
     * @param string $countryTitle  optional Country name the location is at
     * @param string $fqn           optional Fully qualified name for the location
     */
    public function __construct(
        $name,
        $geoPoint,
        $id,
        $countryCode = null,
        $region = null,
        $regionCode = null,
        $city = null,
        $regionCapital = null,
        $timezone = null,
        $nationality = null,
        $population = null,
        $currency = null,
        $currencyCode = null,
        $countryTitle = null,
        $fqn = null
    ) {
        parent::__construct($name, $geoPoint, $id);
        $this->countryCode = $countryCode;
        $this->region = $region;
        $this->regionCode = $regionCode;
        $this->city = $city;
        $this->regionCapital = $regionCapital;
        $this->timezone = $timezone;
        $this->nationality = $nationality;
        $this->population = $population;
        $this->currency = $currency;
        $this->currencyCode = $currencyCode;
        $this->countryTitle = $countryTitle;
        $this->fqn = $fqn;
    }

    public function getCountryCode()
    {
        return $this->countryCode;
    }

    public function setCountryCode($countryCode)
    {
        $this->countryCode = $countryCode;

        return $this;
    }

    public function getRegion()
    {
        return $this->region;
    }

    public function setRegion($region)
    {
        $this->region = $region;

        return $this;
    }

    public function getRegionCode()
    {
        return $this->regionCode;
    }

    public function setRegionCode($regionCode)
    {
        $this->regionCode = $regionCode;

        return $this;
    }

    public function getCity()
    {
        return $this->city;
    }

    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    public function getRegionCapital()
    {
        return $this->regionCapital;
    }

    public function setRegionCapital($regionCapital)
    {
        $this->regionCapital = $regionCapital;

        return $this;
    }

    public function getTimezone()
    {
        return $this->timezone;
    }

    public function setTimezone($timezone)
    {
        $this->timezone = $timezone;

        return $this;
    }

    public function getNationality()
    {
        return $this->nationality;
    }

    public function setNationality($nationality)
    {
        $this->nationality = $nationality;

        return $this;
    }

    public function getPopulation()
    {
        return $this->population;
    }

    public function setPopulation($population)
    {
        $this->population = $population;

        return $this;
    }

    public function getCurrency()
    {
        return $this->currency;
    }

    public function setCurrency($currency)
    {
        $this->currency = $currency;

        return $this;
    }

    public function getCurrencyCode()
    {
        return $this->currencyCode;
    }

    public function setCurrencyCode($currencyCode)
    {
        $this->currencyCode = $currencyCode;

        return $this;
    }

    public function getCountryTitle()
    {
        return $this->countryTitle;
    }

    public function setCountryTitle($countryTitle)
    {
        $this->countryTitle = $countryTitle;

        return $this;
    }

    public function getFqn()
    {
        return $this->fqn;
    }

    public function setFqn($fqn)
    {
        $this->fqn = $fqn;

        return $this;
    }

}
