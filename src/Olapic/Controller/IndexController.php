<?php
/**
 * This file is part of the Olapic Backend Test Project, and contains the
 * IndexController controller class implementation.
 *
 * @author Agustin Berasategui <ajberasategui@gmail.com>
 *
 * @package Olapic\Controller;
 */

namespace Olapic\Controller;

/**
 * Implements the controller to respond to root requests (/).
 */
class IndexController
{
    /**
     *
     * @var Application main Silex Application instance.
     */
    protected $app;

    /**
     * Controller constructor.
     * @param Symfony\Component\HttpFoundation\Request $reqServ
     * @param Silex\Provider\TwigServiceProvider $twigServ
     * @param Silex\Provider\SerializerServiceProvider $serialServ
     */
    public function __construct($app)
    {
        $this->app = $app;
    }

    /**
     * Responds to root url requests with usage instructions
     * @return string JSON/XML encoded usage instructions
     * @return string HTML rendered template with usage instructions
     */
    public function showUsage()
    {
        $acceptHeader = $this->app['request']->headers->get('Accept');
        $acceptFormat = $this->app['negotiator']->getBest(
            $acceptHeader,
            ['text/html', 'application/json', 'xml']
        );
        if (null == $acceptFormat) {
            $this->app->abort(400, 'Response Accept format "' . $acceptHeader .
                '" not supported');
        } else {
            $requestUri = $this->app['request']->getUri();
            $format = $acceptFormat->getValue();

            if ('text/html' == $format) {
                return $this->app['twig']->render('index.twig', [
                    'requestUri'    => $requestUri
                ]);
            } else if (
                'application/json' == $format ||
                'application/xml' == $format
            ) {
                $usageMessage = [
                    'Usage' => 'Make a GET request to ' . $requestUri .
                        '{mediaId}?token=yourInstagramAccessToken',
                    'JSON'  => 'To get a JSON response set your request ' .
                        'Accept header to application/json',
                    'XML'   => 'To get a XML response set your request ' .
                        'Accept header to application/xml'
                ];
                switch ($format) {
                    case 'application/json':
                        return $this->app->json($usageMessage);
                        break;
                    case 'application/xml':
                        return $app['serializer']->serialize(
                            $usageMessage,
                            'xml'
                        );
                        break;
                }
            } else {
                $this->app->abort("Request Accept format not supported", 400);
            }
        }
    }
}
