<?php
/**
 * This file contains the PHPUnit Unit tests for the
 * Olapic\Provider\LocationServiceProvider service.
 *
 * @author Agustin Berasategui <ajberasategui@gmail.com>
 *
 * @package Olapic\Tests
 */

namespace Olapic\Tests;

use Silex\Application;
use Olapic\Model\Media;
use Olapic\Provider\LocationServiceProvider;
use Olapic\Model\ExtendedLocation;

/**
 * This class implements Unit test cases for the LocationServiceProvider service
 * class.
 */
class LocationServiceTest extends \PHPUnit_Framework_TestCase
{
    /**
     * Main Silex Application reference.
     * @var Application
     */
    protected $app = null;
    /**
     * ID of the image to retrieve the location data.
     * @var string
     */
    protected $mediaId = null;
    /**
     * Access Token mock for testing.
     * @var string
     */
    protected $accessToken = null;
    /**
     * Endpoint mock for testing.
     * @var string
     */
    protected $endpoint = null;

    public function setUp()
    {
        parent::setUp();
        $this->app = new Application();
        $this->mediaId = '914837609611565028_425596060';
        $this->endpoint = "testEndpoint";
        $this->accessToken = 'aValidToken';
    }

    /**
     * This method tests that LocationServiceProvider method getLocation
     * - returns an instance of Olapic\Model\ExtendedLocation model class,
     * - the getLocationFromInstagram method is called once
     * - the getLocationFromInstagram method is called with the correct
     * argument.
     *
     * @uses $this->getTestLocationResponse
     */
    public function testGetLocationByIDReturnsLocationObject()
    {
        $locationService = $this->getMockBuilder(
                                    'Olapic\Provider\LocationServiceProvider'
                                )
                                ->setMethods(['getLocationFromInstagram'])
                                ->getMock();

        $decodedResponse = $this->getTestLocationResponse();

        $locationService->method('getLocationFromInstagram')
                        ->will($this->returnValue($decodedResponse));

        $expectedUri =
            $this->endpoint .
            $this->mediaId .
            "/?access_token=" .
            $this->accessToken;

        $locationService->expects($this->once())
                        ->method('getLocationFromInstagram')
                        ->with($this->equalTo($expectedUri));

        $location = $locationService->getLocation(
            $this->endpoint,
            $this->accessToken,
            $this->mediaId
        );

        $this->assertInstanceOf('Olapic\Model\Location', $location);
    }

    /**
     * In this case we mock the getLocationFromGeoBytes method to avoid the
     * actual interaction with the external API and we test that the service
     * getExtraLocation method returns an ExtendedLocation class instance
     * with its geoPoint attribute correctly set.
     *
     * @uses $this->getTestGeoBytesResponse
     */
    public function testGetExtraLocationByIDReturnsExtendedLocation()
    {
        $locationService = $this->getMockBuilder(
                                    'Olapic\Provider\LocationServiceProvider'
                                )
                                ->setMethods(['getLocationFromGeoBytes'])
                                ->getMock();

        $decodedResponse = $this->getTestGeoBytesResponse();

        $testLatitude = "36.104980522";
        $testLongitude = "-115.178432177";

        $locationService->method('getLocationFromGeoBytes')
                        ->will($this->returnValue($decodedResponse));

        $locationService->expects($this->once())
                        ->method('getLocationFromGeoBytes')
                        ->with(
                            $this->equalTo($testLatitude),
                            $this->equalTo($testLongitude)
                        );

        $extLocation = $locationService->getExtraLocation(
            $testLatitude,
            $testLongitude,
            "Test location Name",
            "TestID"
        );

        // We test the service method completed succesfully and returned an
        // instace of ExtendedLocation class
        $this->assertInstanceOf('Olapic\Model\ExtendedLocation', $extLocation);

        // We test the returned ExtendedLocation instance has the correct
        // geo point set.
        $this->assertEquals(
            $testLatitude,
            $extLocation->getGeoPoint()['latitude']
        );
        $this->assertEquals(
            $testLongitude,
            $extLocation->getGeoPoint()['longitude']
        );
    }

    /**
     * Olapic\Provider\LocationServiceProvider->getLocationFromInstagram mock
     * which returns an stdClass object as that method would do but without
     * making any actual external request.
     *
     * @return stdClass An stdClass object containing the attributes a decoded
     * response from Instagram would contain.
     */
    protected function getTestLocationResponse()
    {
        $decodedResponse = new \stdClass();
        $location = new \stdClass();
        $location->location = new \stdClass();
        $location->location->id = "test id";
        $location->location->name = "test name";
        $location->location->latitude = "36.104980522";
        $location->location->longitude = "-115.178432177";
        $location->images = new \stdClass();
        $location->images->low_resolution = new \stdClass();
        $location->images->low_resolution->url = "test url";
        $decodedResponse->data = $location;
        return $decodedResponse;
    }

    /**
     * Olapic\Provider\LocationServiceProvider->getLocationFromGeoBytes mock
     * which returns an stdClass object as that method would do but without
     * making any actual external request.
     *
     * @return stdClass An stdClass object containing the attributes a decoded
     * response from GeoBytes would contain.
     */
    protected function getTestGeoBytesResponse()
    {
        $decodedResponse = new \stdClass();
        $decodedResponse->geobytesinternet = "test internet";
        $decodedResponse->geobytesregion = "test region";
        $decodedResponse->geobytescode = "test code";
        $decodedResponse->geobytescity = "test city";
        $decodedResponse->geobytescapital = "test capital";
        $decodedResponse->geobytestimezone = "test timezone";
        $decodedResponse->geobytesnationalitysingular = "test nationality";
        $decodedResponse->geobytespopulation = "test population";
        $decodedResponse->geobytescurrency = "test currency";
        $decodedResponse->geobytescurrencycode = "test currency code";
        $decodedResponse->geobytestitle = "test title";
        $decodedResponse->geobytesfqcn = "test fqcn";
        return $decodedResponse;
    }
}
