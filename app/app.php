<?php
/**
 * This file contains the main Silex Application object instantation and
 * initialization.
 *
 * @author Agustin Berasategui <ajberasategui@gmail.com>
 */

// TODO: Remove hardcoded token

error_reporting(E_ALL);

require_once __DIR__.'/../vendor/autoload.php';

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Silex\Provider\SerializerServiceProvider;
use Silex\Provider\TwigServiceProvider;
use Silex\Provider\ServiceControllerServiceProvider;
use KPhoen\Provider\NegotiationServiceProvider;
use Olapic\Model\Media;
use Olapic\Provider\LocationServiceProvider;
use Olapic\Provider\IdServiceProvider;
use Olapic\Controller\IndexController;
use Olapic\Provider\LocationControllerProvider;

$app = new Silex\Application();
$app['debug'] = true;

/* Register Services */
$app->register(new ServiceControllerServiceProvider());
$app->register(new LocationServiceProvider(), [
    'location.endpoint' => 'https://api.instagram.com/v1/media/'
]);
$app->register(new IdServiceProvider());
$app->register(new SerializerServiceProvider());
$app->register(new TwigServiceProvider(), [
    'twig.path' => __DIR__ . '/../src/Olapic/View'
]);

$app->register(new NegotiationServiceProvider());

/* Routing */
$app['index.controller'] = $app->share(function() use ($app) {
    return new IndexController($app);
});
$app->get('/', "index.controller:showUsage");
$app->mount('/location', new LocationControllerProvider());

/* Error Handling */
$app->error(function (\Exception $e, $code) use($app) {
    $acceptHeader = $app['request']->headers->get('Accept');
    $acceptFormat = $app['negotiator']->getBest(
        $acceptHeader,
        ['text/html', 'application/json', 'xml']
    );
    $message = $e->getMessage();
    if (null != $acceptFormat) {
        $format = $acceptFormat->getValue();
        switch ($format) {
            case "application/json":
                $response = [
                    'code'      => $code,
                    'message'   => $message
                ];
                return new JsonResponse($response, $code);
                break;
            case "text/html":
                return $app['twig']->render('error.twig', [
                    'code'          => $code,
                    'errorMessage'  => $message
                ]);
                break;
            case "application/xml":

                break;
            default:
                return new Response($code, $message);
                break;
        }
    } else {
        return new Response($message, $code);
    }
});

return $app;
