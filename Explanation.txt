Marcos reviewed my test project and it was not good enough for what you were
looking at Olapic.

In his review Marcos included a lot of resources and advises on how to make my
project better. I was not sure if he was offering me a second chance to re
make my project using his advises but, just in case he was, I decided to do it.

First thing I did was to take a look at every resource he mentioned and get an
idea about what was each of them.

Then I decided to use PHP-Fig PSRs to standarize my code and organize it using
namespaces according to its recommendations.

Also, I decided to try using Silex as framework to process the requests and
use the MVC design pattern.

Other resources I decided to use were composer as dependency manager and
loader and Guzzle to make the Instagram API requests.

For the information retrieved I decided I would investigate a way to get
regional information about the media location to make it more complete.

Then I created an empty Bitbucket git repository and added to it the following
starting files:
- .gitignore: configured to ignore /vendor directory.
- composer.json with the dependencies I needed and autoload namespace
configuration for my src code.
- Readme.md: to write usage instructions later.
- Explanation.txt: this file containing the explanation of the process I
followed to create my solution.

By the way, I also configured my editor to use PSR recommendations for
indentation and column width.

At this point I commited my changes for first time.

After the first commit I ran 'composer update' (considering I have composer
system wide installed) to install project dependencies and created the
src/Olipc directory to contain my project files.
I also decided to add composer.phar to my project root folder so when someone
wants to use this project does not need to install or download it.

Then, taking into account Silex framework MVC structure and the problem domain,
I designed the architectural model of my solution.

I decided that I would need the following classes:
- Olapic\Controller\LocationController: would control the location requests.

- Olapic\Model\Location would model and represent the media location
information.

- Olapic\Provider\LocationServiceProvider implementing
Silex\ServiceProviderInterface, would interact with Instagram API to obtain the
requested media location data and probably with other APIs to obtain regional
information.

- Olapic\View\Location Will be used to show location data in html format.

With the model finished, I started creating the directories and files to
implement them. After this creation I commited my work again.

Next step was to start writing what I considered the core of the application,
which is the LocationServiceProvider implementation. To do this, the first
thing I did was to open the Silex documentation about Service Providers /
ServiceProviderInterface interface. I found that I needed to implement the
interface methods register and boot, so I read what were they supposed to do.
The register method was supposed to define the services this provider would
provide and the logic that should be executed when calling any of those
services. Boot was supposed to make any necessary configurations before the
services handles any request.

So, I decided that my provider needed to provide a "location" service which
would take at register time the following parameters:
- Instagram API token
- Instagram API endpoint

Calling the service itself after booted would need a parameter for the media ID.

Then I started implementing this service provider class. The first thing I
needed to implement was a service with name "location" which would take the
media ID as parameter and would return its location data. Using Guzzle I
implemented the basic structure of the Instagram API interaction to get
media location data. At this point I was correctly processing both correct
responses and bad request responses. The next step should be to get more
location information from another API (which should I research about) and
researching about the correct type my service should return to the controller
according to Silex.
At this moment I commited my work.

After investigating I found that my location service could just return an
instance of my Location Model class.

I also investigated on how to respond to different accept types requested to
Silex. I found that the request format could be set using a query parameter
"[_local=xml | _local=json]" otherwise it would interpret the request accept
format as html. I also found that I could find which format was requested
accessing "$app['request']->getRequestFormat()" and that I could add to my
project the package symfony/serializer to format my response as json or xml and
that I could write a twig template for html response. Then, using all this
knowledge I implemented each response type.
See (*2).

The next step was to investigate http://www.geobytes.com/ API which I found
that could provide a lot of details about a location using its geolocation data.
After investigating it, I started writing a new service in my
LocationServiceProvider called 'extraLocation' to interact with that API and
get the extra information.
After writing some testing requests I commited my work.

I decided then that I would create a model class which extends the Location
class to represent the location with the extra information I could obtain from
GeoBytes API.

I also decided to create a new view and its corresponding controller to show
usage instructions along with a form to make the request of location
information, as well to serve as a fallback for erroneous requests.
For the view I created the twig template src/Olapic/View/index.twig and the
controller src/Olapic/Controller/IndexController.php

I registered in my index.php script the routes for the index controller/view
as well as the LocationControllerProvider routes for /location/ requests.

I had from the previous project the idea of creating a way to obtain the media
location information using its url instead of its ID as its URL is much easier
to obtain. So, using the new architecture provided by Silex I decided to
create a Service which would obtain a Media ID for a given Media URL.
After implementing this service (Olapic\Provider\IdServiceProvider) I added to
the LocationControllerProvider a route handler for the request url
"/location/byurl/{url}" to handle requests of location data by media url.
See (*1).

I also thought that to share the data between services, controllers and views
it was better to create a Media model class which encapsulates all the media
data and its location.

(*1) As this was not consistent with REST API's URI specification I decided to
change the request URL to "/location/?url={url}" (where the url would be passed
as a query parameter). This meant to modify the location controller routes and
methods

The next thing I needed to implement was a proper error handling mechanism
using Silex Error Handlers, so I investigated about it on its documentation
and implemented it accordingly. At this point I also decided that it would be
a better to respond with the corresponding HTTP error codes and messages to
erroneous request instead of redirecting all of them to the usage instructions,
and leave those instructions only accessible by sending a request to the root
url (/).

(*2) Studying Silex error handling I found that I can get the request
Accept header very easily and I consider that it is much more convenient as well
as standard, to use it instead of using the query parameter "_format", so I
decided to modify my code to use it.

While I was writing the Unit Tests for my service I sadly concluded that I was
just saving the location related data obtained from Instagram API and wasting
all the rest of the information which could be used for extending the
functionality later. I'm aware that changing the service at this point is not
a very good idea and that I should have noticed this when I designed the
solution structure. Now I think that it would have been better to write a
MediaServiceProvider which retrieves the data from Instagram's API and
implements a method to obtain just the location from it while keeping the rest
of the data. But as my project is starting to take too long to be finished I
decided not to change it now.
